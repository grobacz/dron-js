const ioHook = require('iohook');

const monitor = {
    debug: true,
    log: function (content) {
        if (!monitor.debug) {
            return;
        }
        if (typeof content === 'object') {
            content = JSON.stringify(content, undefined, 4);
        }
        if (typeof content === 'undefined') {
            content = 'undefined'
        }
        if (typeof content !== 'string') {
            content = content.toString();
        }
        process.stdout.write(content);
    },
    loop: function (state) {
        process.stdout.cursorTo(0, 0);
        process.stdout.clearScreenDown();
        monitor.log(state);
    },
    setup: function () {
        process.stdin.on('keypress', function (chunk, key) {
            if (key.sequence === '\u0003') {
                process.exit();
            }
        });
    }
};

module.exports = monitor;