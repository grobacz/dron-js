const pcKeymap = {
    UP: 57416, // arrow up
    DOWN: 57424, // arrow down
    START: 44, // z
    LAND: 45, // x
    FORWARD: 17, // w
    BACK: 31, // s
    LEFT: 30, // d
    RIGHT: 32, // a
    SPINRIGHT: 18, // e
    SPINLEFT: 16, // q
    DISABLEEMERGENCY: 1, // esc
};

var arDrone = require('ar-drone');
// var client = arDrone.createClient();

var control = arDrone.createUdpControl();

// Readline lets us tap into the process events
const readline = require('readline');

// Allows us to listen for events from stdin
readline.emitKeypressEvents(process.stdin);

const ioHook = require('iohook');

// Raw mode gets rid of standard keypress events and other
// functionality Node.js adds by default
process.stdin.setRawMode(true);

const debug = true;
const log = function (content) {
    if (!debug) {
        return;
    }
    if (typeof content === 'object') {
        content = JSON.stringify(content, undefined, 4);
    }
    if (typeof content === 'number') {
        content = content.toString();
    }
    process.stdout.write(content);
};

process.stdout.cursorTo(0, 0);
process.stdout.clearScreenDown();
log("Lece sobie:");

var speed = 0.1;

var up = false;
var down = false;
var start = false;
var land = false;
var forward = false;
var back = false;
var left = false;
var right = false;
var spinright = false;
var spinleft = false;
var disableEmergency = false;

function setFlag(keyCode, val) {
    let oldVal = null;
    switch (keyCode) {
        case 57416:
            oldVal = up;
            up = val;
            break;
        case 57424:
            oldVal = down;
            down = val;
            break;
        case 17:
            oldVal = forward;
            forward = val;
            break;
        case 31:
            oldVal = back;
            back = val;
            break;
        case 32:
            oldVal = right;
            right = val;
            break;
        case 30:
            oldVal = left;
            left = val;
            break;
        case 18:
            oldVal = spinright;
            spinright = val;
            break;
        case 16:
            oldVal = spinleft;
            spinleft = val;
            break;
    }
    return oldVal === val;
}

ioHook.on("keydown", event => {
    process.stdout.cursorTo(0, 0);
    process.stdout.clearScreenDown();
    log(event.keycode);
    return;
    let controlOp = true;
    switch (event.keycode) {
        case 44:
            start = !start;
            break;
        case 45:
            land = !land;
            break;
        case 1:
            disableEmergency = !disableEmergency;
            break;
        default:
            controlOp = false;
    }

    // console.log('down');
    if (!controlOp) {
        const changed = setFlag(event.keycode, true);

        if (changed) {
            // console.log('changed');
        }
    }
    // result: {keychar: 'f', keycode: 19, rawcode: 15, type: 'keypress'}
});
ioHook.on("keyup", event => {
    // console.log('up');
    setFlag(event.keycode, false);
    // result: {keychar: 'f', keycode: 19, rawcode: 15, type: 'keypress'}
});

ioHook.start();

process.stdin.on('keypress', function (chunk, key) {
    if (key.sequence === '\u0003') {
        process.exit();
    }
});

var fly = function () {
    var ref = {};
    var pcmd = {};

    // pierwsza rzecz to bezpieczne wyłączenie drona
    if (land) {
        ref.fly = false;
    } else if (start) {
        // po drugie start
        ref.fly = true;
    } else if (disableEmergency) {
        ref.fly = false;
        ref.emergency = true;
    }

    // dalej już tylko kontrola lotu
    // zabezpieczam przed sprzecznymi sygnałami jednocześnie
    if (up) {
        pcmd.up = speed;
    } else if (down) {
        pcmd.down = speed;
    }

    if (right) {
        pcmd.right = speed;
    } else if (left) {
        pcmd.left = speed;
    }

    if (forward) {
        pcmd.front = speed;
    } else if (back) {
        pcmd.back = speed;
    }

    if (spinright) {
        pcmd.clockwise = speed;
    } else if (spinleft) {
        pcmd.counterClockwise = speed;
    }

    // console.log(ref);
    // console.log(pcmd);

    control.ref(ref);
    control.pcmd(pcmd);
    control.flush();
};

setInterval(fly, 100);

var http = require('http');

// http.createServer(function (req, res) {
//     var stream = client.getVideoStream();
//     res.writeHead(200, {'Content-Type': 'video/mp4',});
//     stream.pipe(res);
// }).listen(8080);

var monitor = function () {
    process.stdout.cursorTo(0, 1);
    process.stdout.clearScreenDown();
    // log({
    //     "up": up,
    //     "down": down,
    //     "start": start,
    //     "land": land,
    //     "forward": forward,
    //     "back": back,
    //     "left": left,
    //     "right": right,
    //     "spinright": spinright,
    //     "spinleft": spinleft,
    //     "disableEmergency": disableEmergency
    // });
};
setInterval(monitor, 100);