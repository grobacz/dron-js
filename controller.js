var gamepad = require("gamepad");

// Initialize the library
gamepad.init()

// Scan for gamepads
// gamepad.detectDevies();

// Create a game loop and poll for events
setInterval(gamepad.processEvents, 16);

const {STATE, ACTION, CONTROL_ACTIONS, FLY_ACTIONS} = require('./store');

const keymap = {
    3: ACTION.START, // Y
    0: ACTION.LAND, // A
    1: ACTION.DISABLEEMERGENCY, // B
    2: ACTION.SPEED_MULTIPLIER, // X
};

const determinateButtonAction = function (code) {
    return keymap[code];
}

const moveActionDeterminate = function (axis, value) {
    if (axis === 1) {
        return value >=0 ? ACTION.BACK : ACTION.FORWARD;
    } else if (axis === 0) {
        return value >=0 ? ACTION.RIGHT : ACTION.LEFT;
    } else if (axis === 3) {
        return value >=0 ? ACTION.SPINRIGHT : ACTION.SPINLEFT;
    } else if (axis === 4) {
        return value >=0 ? ACTION.DOWN : ACTION.UP;
    }
}

// Listen for move events on all gamepads
gamepad.on("move", function (id, axis, value) {
    if (Math.abs(value) < 0.1) {
        value = 0;
    }
    const action = moveActionDeterminate(axis, value);

    switch (action) {
        case ACTION.UP: STATE.DOWN = 0; break;
        case ACTION.DOWN: STATE.UP = 0; break;
        case ACTION.LEFT: STATE.RIGHT = 0; break;
        case ACTION.RIGHT: STATE.LEFT = 0; break;
        case ACTION.FORWARD: STATE.BACK = 0; break;
        case ACTION.BACK: STATE.FORWARD = 0; break;
        case ACTION.SPINRIGHT: STATE.SPINLEFT = 0; break;
        case ACTION.SPINLEFT: STATE.SPINRIGHT = 0; break;
        default: return;
    }
    STATE[action] = Math.abs(value) * STATE.SPEED_MULTIPLIER;
});

// Listen for button up events on all gamepads
// gamepad.on("up", function (id, num) {
//     console.log("up", {
//         id: id,
//         num: num,
//     });
// });

// Listen for button down events on all gamepads
gamepad.on("down", function (id, num) {
    const action = determinateButtonAction(num);

    if (action === undefined) {
        return;
    }

    if (action === ACTION.SPEED_MULTIPLIER) {
        toggleSpeedMultiplier();
        return;
    }

    for (let i = 0; i < FLY_ACTIONS.length; i++) {
        STATE[FLY_ACTIONS[i]] = 0;
    }
    for (let i = 0; i < CONTROL_ACTIONS.length; i++) {
        STATE[CONTROL_ACTIONS[i]] = false;
    }
    STATE[action] = true;
});

/*
A - down { id: 0, num: 0 }
B - down { id: 0, num: 1 }
X - down { id: 0, num: 2 }
Y - down { id: 0, num: 3 }
LUP - move { id: 0, axis: 1, value: -1 }
LDOWN - move { id: 0, axis: 1, value: 1 }
LLEFT - move { id: 0, axis: 0, value: -1 }
LRIGHT - move { id: 0, axis: 0, value: 1 }
RUP - move { id: 0, axis: 4, value: -1 }
RDOWN - move { id: 0, axis: 4, value: 1 }
RLEFT - move { id: 0, axis: 3, value: -1 }
RRIGHT - move { id: 0, axis: 3, value: 1 }
 */