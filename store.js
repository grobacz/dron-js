const ACTION = {
    UP: 'UP',
    DOWN: 'DOWN',
    START: 'START',
    LAND: 'LAND',
    FORWARD: 'FORWARD',
    BACK: 'BACK',
    LEFT: 'LEFT',
    RIGHT: 'RIGHT',
    SPINRIGHT: 'SPINRIGHT',
    SPINLEFT: 'SPINLEFT',
    DISABLEEMERGENCY: 'DISABLEEMERGENCY',
    SPEED_MULTIPLIER: 'SPEED_MULTIPLIER'
};
const STATE = {
    'UP': 0,
    'DOWN': 0,
    'START': false,
    'LAND': false,
    'FORWARD': 0,
    'BACK': 0,
    'LEFT': 0,
    'RIGHT': 0,
    'SPINRIGHT': 0,
    'SPINLEFT': 0,
    'DISABLEEMERGENCY': false,
    'SPEED_MULTIPLIER': 0.3
};
module.exports = {
    STATE: STATE,
    ACTION: ACTION,
    CONTROL_ACTIONS: [ACTION.START, ACTION.LAND, ACTION.DISABLEEMERGENCY],
    FLY_ACTIONS: [
        ACTION.UP,
        ACTION.DOWN,
        ACTION.FORWARD,
        ACTION.BACK,
        ACTION.LEFT,
        ACTION.RIGHT,
        ACTION.SPINRIGHT,
        ACTION.SPINLEFT
    ],
    toggleSpeedMultiplier: function () {
        const speedThresholds = [
            0.3,
            0.5,
            0.8,
            1.0
        ];
        const currentThresholdIndex = speedThresholds.indexOf(STATE.SPEED_MULTIPLIER);
        STATE.SPEED_MULTIPLIER = currentThresholdIndex + 1 === speedThresholds.length
            ? speedThresholds[0]
            : speedThresholds[currentThresholdIndex + 1];
    },
};