const {STATE, ACTION, CONTROL_ACTIONS, FLY_ACTIONS, toggleSpeedMultiplier} = require('./store');

const pcKeymap = {
    57416: ACTION.UP, // arrow up
    57424: ACTION.DOWN, // arrow down
    44: ACTION.START, // z
    45: ACTION.LAND, // x
    17: ACTION.FORWARD, // w
    31: ACTION.BACK, // s
    30: ACTION.LEFT, // d
    32: ACTION.RIGHT, // a
    18: ACTION.SPINRIGHT, // e
    16: ACTION.SPINLEFT, // q
    1: ACTION.DISABLEEMERGENCY, // esc
    46: ACTION.SPEED_MULTIPLIER, // c
};

const determinateAction = function (code) {
    return pcKeymap[code];
}

const ioHook = require('iohook');

// speedUp
setInterval(function () {
    for (let i = 0; i < FLY_ACTIONS.length; i++) {
        const currentValue = STATE[FLY_ACTIONS[i]];
        if (currentValue === 0 || currentValue === 1) {
            continue;
        }
        // change speed slowly but faster in lower max speed - it should not get out of control?
        STATE[FLY_ACTIONS[i]] = STATE[FLY_ACTIONS[i]] + 0.05 + 0.5 * (1 - STATE.SPEED_MULTIPLIER);
        if (STATE[FLY_ACTIONS[i]] > STATE.SPEED_MULTIPLIER) {
            STATE[FLY_ACTIONS[i]] = STATE.SPEED_MULTIPLIER;
        }
    }
}, 400);

ioHook.on("keydown", event => {
    const action = determinateAction(event.keycode);
    if (action === undefined) {
        return;
    }
    if (action === ACTION.SPEED_MULTIPLIER) {
        toggleSpeedMultiplier();
    } else if (CONTROL_ACTIONS.indexOf(action) >= 0) {
        for (let i = 0; i < FLY_ACTIONS.length; i++) {
            STATE[FLY_ACTIONS[i]] = 0;
        }
        for (let i = 0; i < CONTROL_ACTIONS.length; i++) {
            STATE[CONTROL_ACTIONS[i]] = false;
        }
        STATE[action] = true;
    } else if (STATE[action] === 0) {
        STATE[action] = 0.1;
    }
});
ioHook.on("keyup", event => {
    const action = determinateAction(event.keycode);
    if (action === undefined) {
        return;
    }
    if (CONTROL_ACTIONS.indexOf(action) >= 0 || action === ACTION.SPEED_MULTIPLIER) {
        return;
    }
    STATE[action] = 0;
});

ioHook.start();