const arDrone = require('ar-drone');
const control = arDrone.createUdpControl();
const client = arDrone.createClient();
// update max altitude to 100m
// client.config('control:altitude_max', 100000);

module.exports = function (STATE) {
    var ref = {};
    var pcmd = {};

    if (!STATE.START || STATE.LAND || STATE.DISABLEEMERGENCY) {
        // pierwsza rzecz to bezpieczne wyłączenie drona, natychmiastowy flush
        if (STATE.LAND) {
            ref.fly = false;
        } else if (STATE.DISABLEEMERGENCY) {
            ref.fly = false;
            ref.emergency = true;
            control.ref(ref);
            control.pcmd(pcmd);
            control.flush();
        }

        control.ref(ref);
        control.pcmd(pcmd);
        control.flush();
        return;
    }

    ref.fly = true;

    // dalej już tylko kontrola lotu
    // zabezpieczam przed sprzecznymi sygnałami jednocześnie
    if (STATE.UP > 0) {
        pcmd.up = STATE.UP.toFixed(1);
    } else if (STATE.DOWN > 0) {
        pcmd.down = STATE.DOWN.toFixed(1);
    }

    if (STATE.RIGHT > 0) {
        pcmd.right = STATE.RIGHT.toFixed(1);
    } else if (STATE.LEFT > 0) {
        pcmd.left = STATE.LEFT.toFixed(1);
    }

    if (STATE.FORWARD > 0) {
        pcmd.front = STATE.FORWARD.toFixed(1);
    } else if (STATE.BACK > 0) {
        pcmd.back = STATE.BACK.toFixed(1);
    }

    if (STATE.SPINRIGHT > 0) {
        pcmd.clockwise = STATE.SPINRIGHT;
    } else if (STATE.SPINLEFT > 0) {
        pcmd.counterClockwise = STATE.SPINLEFT;
    }

    control.ref(ref);
    control.pcmd(pcmd);
    control.flush();
};