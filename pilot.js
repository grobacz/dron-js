const yargs = require('yargs');

const argv = yargs
    .command('pc', 'Fly with keyboard')
    .command('xbox', 'Fly with xbox controller')
    .option('dry', {
        alias: 'd',
        description: 'Does not send commands to drone',
        type: 'boolean',
    })
    .option('video', {
        alias: 'v',
        description: 'Starts video feed in browser',
        type: 'boolean',
    })
    .help()
    .alias('help', 'h')
    .argv;

if (argv._.includes('pc')) {
    require('./pc');
} else if (argv._.includes('xbox')) {
    require('./controller');
} else {
    console.log('Provide piloting protocol: pc|xbox');
    return;
}

const MONITOR = require('./monitor');
// Readline lets us tap into the process events
const readline = require('readline');
// Allows us to listen for events from stdin
readline.emitKeypressEvents(process.stdin);
// Raw mode gets rid of standard keypress events and other
// functionality Node.js adds by default
process.stdin.setRawMode(true);
MONITOR.setup();

const {STATE} = require('./store');
setInterval(function(){MONITOR.loop(STATE)}, 16);

if (argv.video) {
    const server = require('./video');
}

if (!argv.dry) {
    const fly = require('./fly')
    setInterval(function(){fly(STATE)}, 50);
}
