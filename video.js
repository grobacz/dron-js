// todo: rozkminić https://github.com/bkw/node-dronestream

var http = require("http"),
    drone = require("dronestream");


var server = http.createServer(function(req, res) {
    require("fs").createReadStream(__dirname + "/index.html").pipe(res);
});

drone.listen(server, { ip: "192.168.1.1" });
server.listen(3000);

const open = require('open');

(async () => {
    await open('http://localhost:3000');
})();